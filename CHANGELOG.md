## [v0.3] - 2022-01-12

### Added

- Add namespace names to CVE summary

### Fixed

- Always validate trivyScan object before running.

### Docker image

`thesmcl/trivylive:v0.3`

### Instructions to upgrade

- Edit the `trivylive` deployment and change the `image:` option

```
$ kubectl edit deployment trivylive -n trivylive
...
    image: thesmcl/trivylive:v0.3
```

## [v0.1] - 2022-01-02

 
### Added

- Added CVE summary to easily see all images affected by a specific CVE.
- Check if newer images exist and add that information to the report.
- Generate html diff for latest summaries.
- Implement retention.
- Avoid duplicates in summary.json.
- Allow report format to json or table.
- Ability to perform hourly scanning.
- Added `release` target in Makefile
 
### Changed
  
- Dedup images to be scanned in the `runScan()` method.
 
### Fixed
 
- Use temp directories in TestWrite.
- Log error when file rename fails. 
- Only delete directories during retention processing.

### Docker image
`thesmcl/trivylive:v0.1`

### Instructions to upgrade

- Edit the `trivylive` deployment and change the `image:` option

```
$ kubectl edit deployment trivylive -n trivylive
...
    image: thesmcl/trivylive:v1.0
```
