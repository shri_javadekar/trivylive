# Build the manager binary
FROM golang:1.16 as builder

WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go env -w GO111MODULE=on GOPROXY=direct && go mod download

# Copy the go source
COPY main.go main.go
COPY api/ api/
COPY controllers/ controllers/
COPY lib/ lib/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o manager main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM debian:buster-slim

RUN apt-get update -y && apt-get install wget -y
RUN wget https://github.com/aquasecurity/trivy/releases/download/v0.21.2/trivy_0.21.2_Linux-64bit.deb
RUN dpkg -i trivy_0.21.2_Linux-64bit.deb

## Install diff2html
RUN apt-get install npm -y
RUN npm install -g diff2html-cli

# Install sinker
RUN wget https://github.com/plexsystems/sinker/releases/download/v0.15.0/sinker-linux-amd64
RUN chmod 755 sinker-linux-amd64 && mv sinker-linux-amd64 /usr/local/bin/sinker

WORKDIR /
RUN mkdir /.cache
RUN chmod 777 /.cache
COPY --from=builder /workspace/manager .
USER 65532:65532

ENTRYPOINT ["/manager"]
