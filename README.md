# TrivyLive

> Kubernetes native periodic scan of all images in a cluster

TrivyLive provides a Kubernetes native mechanism for performing security scans of images in a cluster using a CRD and a controller.


## Installation

- Install the CRD

```
$ kubectl apply -f https://gitlab.com/shri_javadekar/trivylive/-/raw/main/install/crd.yaml
```

- Create the namespace, RBAC, deployment, etc.

```
$ kubectl create ns trivylive
$ kubectl apply -f https://gitlab.com/shri_javadekar/trivylive/-/raw/main/install/trivylive.yaml -n trivylive
```

- Create the custom resource

```
$ kubectl apply -f https://gitlab.com/shri_javadekar/trivylive/-/raw/main/install/trivyscan.yaml -n trivylive
```

## Get results out


- The scan results are stored in a directory and can be accessed via the nginx sidecar container.
- Export the web interface by port-forwarding the svc: `kubectl port-forward svc/trivylive-svc 8080:8080 -n trivylive`
- Visit `http://localhost:8080/` to see the directory with the scan results.

## Build and test


- Download and install [Trivy](https://aquasecurity.github.io/trivy/v0.21.2/getting-started/installation/).

- To run the unit tests, simply run `make test`
- To create the docker image, run `make docker-build`

## Design

The following shows the flow-chart of TrivyLive. Tl;dr: It's super simple

![TrivyDesign](./docs/TrivyDesign.png)

## TODOs

- Create global summary so that high level stats such as total vulnerabilities, etc. can be seen.
- Make the severity (TRIVY_SEVERITY) configurable. Maybe make it part of the CRD itself.
- Convert the json reports into HTML pages
- Make it easy to quickly query/sort vulnerabilities
- Allowed/Denied list of namespaces