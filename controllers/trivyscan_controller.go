package controllers

import (
	"context"
	"errors"
	"time"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	v1alpha1 "amplifykube.com/trivylive/api/v1alpha1"
	"amplifykube.com/trivylive/lib/report"
	"amplifykube.com/trivylive/lib/scan"
	"amplifykube.com/trivylive/lib/setup"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type TrivyLiveGlobalOpts struct {
	ScanToTickerMap          map[types.NamespacedName]chan struct{}
	DefaultScanResultsFSPath string
}

// TrivyScanReconciler reconciles a TrivyScan object
type TrivyScanReconciler struct {
	client.Client
	ClientSet           kubernetes.Interface
	Log                 logr.Logger
	Scheme              *runtime.Scheme
	TrivyLiveGlobalOpts *TrivyLiveGlobalOpts
}

func (r *TrivyScanReconciler) validate(trivyScan *v1alpha1.TrivyScan) bool {
	if trivyScan.Spec.Report.Format == "" {
		trivyScan.Spec.Report.Format = v1alpha1.ReportFormatJson
	}

	if trivyScan.Spec.Report.Format != v1alpha1.ReportFormatJson &&
		trivyScan.Spec.Report.Format != v1alpha1.ReportFormatTable {
		r.Log.Info("Report format should be either json or table")
		return false
	}

	if trivyScan.Spec.Report.Type == "" {
		trivyScan.Spec.Report.Type = v1alpha1.FileSystemType
	}

	if trivyScan.Spec.Report.Type != v1alpha1.FileSystemType {
		r.Log.Info("Only filesystem report type is supported so far")
		return false
	}

	if trivyScan.Spec.Report.Path == "" {
		trivyScan.Spec.Report.Path = r.TrivyLiveGlobalOpts.DefaultScanResultsFSPath
	}
	return true
}

//+kubebuilder:rbac:groups=security.amplifykube.com,resources=trivyscans,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=security.amplifykube.com,resources=trivyscans/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=security.amplifykube.com,resources=trivyscans/finalizers,verbs=update

func (r *TrivyScanReconciler) runScan(ctx context.Context, trivyScan *v1alpha1.TrivyScan) {
	r.Log.Info("Starting to run full scan")

	if !r.validate(trivyScan) {
		r.Log.Error(errors.New("TrivyScan object is invalid"), "Failed to run scan")
		return
	}

	scanStartTime := time.Now()

	clientSet := r.ClientSet
	namespaces, err := clientSet.CoreV1().Namespaces().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		r.Log.Error(err, "failed to list namespaces", "trivyScan", trivyScan.Name)
		return
	}

	var nsReports []v1alpha1.NamespaceVulnerabilitySummary
	scannedImages := make(map[string]bool)
	for _, namespace := range namespaces.Items {
		var imageScanSummaries []v1alpha1.ImageVulnerabilitySummary
		r.Log.Info("Scanning namespace", "namespaceName", namespace.Name)

		// Setup
		prefix, err := setup.Run(namespace.Name, trivyScan.Spec.Report.Path, trivyScan.Spec.Retention.Last, r.Log)
		if err != nil {
			r.Log.Error(err, "failed to create setup")
			return
		}

		pods, err := clientSet.CoreV1().Pods(namespace.Name).List(context.Background(), metav1.ListOptions{})
		if err != nil {
			r.Log.Error(err, "failed to list pods in namespace "+namespace.Name, "trivyScan", trivyScan.Name)
			return
		}
		for _, pod := range pods.Items {
			for _, cStatus := range pod.Status.ContainerStatuses {
				r.Log.Info("Scanning container", "image", cStatus.Image)

				if ok := scannedImages[cStatus.ImageID]; ok {
					r.Log.Info("Image already scanned", "image", cStatus.Image, "imageID", cStatus.ImageID)
					continue
				}

				// Scan
				ivs := scan.Run(cStatus.Image, cStatus.ImageID, prefix, trivyScan.Spec.Report.Format, r.Log)
				if ivs == nil {
					r.Log.Info("Scan returned no results", "image", cStatus.Image)
					continue
				}

				imageScanSummaries = append(imageScanSummaries, *ivs)
				scannedImages[cStatus.ImageID] = true
			}
		}

		// Write the report
		nsVS, err := report.WriteNSReport(namespace.Name, prefix, &imageScanSummaries, r.Log)
		if err != nil {
			r.Log.Error(err, "failed to write summary report", "namespace", nsVS.Namespace)
			continue
		}
		r.Log.Info("Wrote report for namespace", "name", namespace.Name)
		nsReports = append(nsReports, *nsVS)
	}

	r.Log.Info("Writing overall report")
	report.WriteOverallReport(scanStartTime, &nsReports, trivyScan.Spec.Report.Path, r.Log)
}

func (r *TrivyScanReconciler) runScanTicker(ctx context.Context, trivyScan *v1alpha1.TrivyScan) chan struct{} {
	requeueDuration, err := time.ParseDuration(trivyScan.Spec.RepeatDuration)
	if err != nil {
		r.Log.Error(err, "failed to parse repeatDuration")
		return nil
	}
	ticker := time.NewTicker(requeueDuration)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				r.runScan(ctx, trivyScan)
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()

	return quit
}

func (r *TrivyScanReconciler) setupScan(ctx context.Context, trivyScan *v1alpha1.TrivyScan) {
	// Check if the TrivyScan object is already in the map (and therefore started running scans)
	namespacedName := types.NamespacedName{
		Namespace: trivyScan.Namespace,
		Name:      trivyScan.Name,
	}
	_, ok := r.TrivyLiveGlobalOpts.ScanToTickerMap[namespacedName]
	if ok {
		r.Log.Info("scan setup already. Nothing to do")
		return
	}
	// Object not in the map.
	r.TrivyLiveGlobalOpts.ScanToTickerMap[namespacedName] = r.runScanTicker(ctx, trivyScan)
	r.Log.Info("scan setup for running periodically")

	// Run this once here. Tickers only start after the first duration has passed.
	r.runScan(ctx, trivyScan)
}

func (r *TrivyScanReconciler) handleObjectNotFound(err error, req ctrl.Request) (ctrl.Result, error) {
	if k8serrors.IsNotFound(err) {
		r.Log.Info("Object deleted")

		// Stop the ticker and delete the entry from the map
		if quitChan, ok := r.TrivyLiveGlobalOpts.ScanToTickerMap[req.NamespacedName]; ok {
			r.Log.Info("Stopped the scanning")
			if quitChan != nil {
				close(quitChan)
			}
			delete(r.TrivyLiveGlobalOpts.ScanToTickerMap, req.NamespacedName)
		} else {
			r.Log.Info("Quit not found. Scanning may continue")
		}
		return ctrl.Result{}, nil
	}
	r.Log.Error(err, "Failed to get object", "cr", req.NamespacedName)
	return ctrl.Result{}, err
}

// Reconcile is called for every custom resource created/updated in the cluster
func (r *TrivyScanReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	r.Log = r.Log.WithValues("TrivyScan", req.NamespacedName)

	r.Log.Info("Reconciling")

	// Fetch the TrivyScan object
	trivyScan := &v1alpha1.TrivyScan{}
	err := r.Get(ctx, req.NamespacedName, trivyScan)
	if err != nil {
		return r.handleObjectNotFound(err, req)
	}

	if !r.validate(trivyScan) {
		r.Log.Error(errors.New("TrivyScan object is invalid"), "Failed to run scan")
		return ctrl.Result{}, err
	}

	r.setupScan(ctx, trivyScan)

	trivyScan.Status.State = v1alpha1.TrivyScanRunning
	r.Status().Update(ctx, trivyScan)

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *TrivyScanReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha1.TrivyScan{}).
		WithEventFilter(predicate.Funcs{
			UpdateFunc: func(e event.UpdateEvent) bool {
				oldGeneration := e.ObjectOld.GetGeneration()
				newGeneration := e.ObjectNew.GetGeneration()
				// Generation is only updated on spec changes (also on deletion),
				// not metadata or status
				// Filter out events where the generation hasn't changed to
				// avoid being triggered by status updates
				return oldGeneration != newGeneration
			},
		}).
		Complete(r)
}
