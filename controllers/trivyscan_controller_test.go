package controllers

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"amplifykube.com/trivylive/api/v1alpha1"
	"amplifykube.com/trivylive/lib/report"
	"github.com/onsi/gomega"
	coreV1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/fake"
	"k8s.io/klog/klogr"
	ctrl "sigs.k8s.io/controller-runtime"
)

func getPodList(maxPods int, nsName string) *coreV1.PodList {
	pods := coreV1.PodList{}
	for i := 1; i <= maxPods; i++ {
		var restartCount int32
		if i%7 == 0 {
			restartCount = 4
		}
		pod := coreV1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      fmt.Sprintf("mypod-%d", i),
				Namespace: nsName,
			},
			Status: coreV1.PodStatus{
				ContainerStatuses: []coreV1.ContainerStatus{
					coreV1.ContainerStatus{
						RestartCount: restartCount,
						Image:        "nginx:latest",
						ImageID:      "sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85",
					},
					coreV1.ContainerStatus{
						RestartCount: restartCount,
						Image:        "nginx:latest",
						ImageID:      "sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85",
					},
				},
			},
		}

		pods.Items = append(pods.Items, pod)
	}

	return &pods
}
func TestRunScan(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	testNs := "testNs"

	ctrl.SetLogger(klogr.New())

	ns := coreV1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: testNs}}
	clientset := fake.NewSimpleClientset(&coreV1.NamespaceList{
		Items: []coreV1.Namespace{ns},
	}, getPodList(2, testNs))

	trivyScanToTickerMap := make(map[types.NamespacedName]chan struct{})

	dirPath, err := ioutil.TempDir("/tmp", "trivy-run-scan-")
	if err != nil {
		t.Fail()
	}
	defer os.RemoveAll(dirPath)

	var TrivyLiveGlobalOpts TrivyLiveGlobalOpts
	TrivyLiveGlobalOpts.ScanToTickerMap = trivyScanToTickerMap
	r := &TrivyScanReconciler{
		ClientSet:           clientset,
		Log:                 ctrl.Log.WithName("controllers").WithName("TrivyScan"),
		TrivyLiveGlobalOpts: &TrivyLiveGlobalOpts,
	}

	trivyScan := v1alpha1.TrivyScan{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "my-test-trivyscan",
			Namespace: testNs,
		},
		Spec: v1alpha1.TrivyScanSpec{
			RepeatDuration: "2h",
			Report: v1alpha1.ReportDetails{
				Path:   dirPath,
				Format: "table",
			},
			Retention: v1alpha1.RetentionDetails{},
		},
	}

	r.runScan(context.Background(), &trivyScan)

	// The reports should be present.
	fileInfo, err := os.Stat(filepath.Join(dirPath, report.LastScanSummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Log("Latest scan summary did not get created")
		t.Fail()
	}
	// 79 bytes is the size of the summary when there is only a single image entry.
	g.Expect(fileInfo.Size()).To(gomega.Equal(int64(210)))

	fileInfo, err = os.Stat(filepath.Join(dirPath, report.CVESummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Log("CVESummary did not get created")
		t.Fail()
	}
	// 86 bytes is the size of the cve summary when there is only a single image entry.
	g.Expect(fileInfo.Size()).To(gomega.Equal(int64(86)))
}

func TestValidate(t *testing.T) {
	g := gomega.NewGomegaWithT(t)
	trivyScanToTickerMap := make(map[types.NamespacedName]chan struct{})

	ctrl.SetLogger(klogr.New())

	var TrivyLiveGlobalOpts TrivyLiveGlobalOpts
	TrivyLiveGlobalOpts.ScanToTickerMap = trivyScanToTickerMap
	TrivyLiveGlobalOpts.DefaultScanResultsFSPath = "/trivylive-scan-results"

	r := &TrivyScanReconciler{
		Log:                 ctrl.Log.WithName("controllers").WithName("TrivyScan"),
		TrivyLiveGlobalOpts: &TrivyLiveGlobalOpts,
	}

	trivyScan := v1alpha1.TrivyScan{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "my-test-trivyscan",
			Namespace: "testNS",
		},
		Spec: v1alpha1.TrivyScanSpec{
			RepeatDuration: "2h",
			Report:         v1alpha1.ReportDetails{},
			Retention:      v1alpha1.RetentionDetails{},
		},
	}

	// Empty report format. Should set the default to json format
	r.validate(&trivyScan)
	g.Expect(trivyScan.Spec.Report.Format).To(gomega.Equal(v1alpha1.ReportFormatJson))

	// Call validate without any report type. Validate should succeed and the report type should be filesystem
	g.Expect(r.validate(&trivyScan)).To(gomega.BeTrue())
	g.Expect(trivyScan.Spec.Report.Type).To(gomega.Equal(v1alpha1.FileSystemType))
	g.Expect(trivyScan.Spec.Report.Path).To(gomega.Equal(TrivyLiveGlobalOpts.DefaultScanResultsFSPath))

	// Set the report format to some other than json or table.
	trivyScan.Spec.Report.Format = "abcd"
	g.Expect(r.validate(&trivyScan)).To(gomega.BeFalse())

	trivyScan.Spec.Report.Format = "json"
	trivyScan.Spec.Report.Type = "s3"
	g.Expect(r.validate(&trivyScan)).To(gomega.BeFalse())

	trivyScan.Spec.Report.Format = "table"
	trivyScan.Spec.Report.Type = "s3"
	g.Expect(r.validate(&trivyScan)).To(gomega.BeFalse())

	trivyScan.Spec.Report.Format = "json"
	trivyScan.Spec.Report.Type = v1alpha1.FileSystemType
	g.Expect(r.validate(&trivyScan)).To(gomega.BeTrue())
}
