package image

import (
	"os/exec"
	"strings"

	"github.com/go-logr/logr"
)

func CheckNewVersion(image string, log logr.Logger) string {
	cmd := exec.Command("sinker", "check", "--images", image)

	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Error(err, "Failed to run image version check", "image", image)
		log.Info(string(out))
		return ""
	}

	sinkerOuts := strings.Split(string(out), "msg=")
	if len(sinkerOuts) > 1 {
		outputStr := string(sinkerOuts[1])
		// Remove any newline and quotes
		outputStr = strings.TrimSuffix(outputStr, "\n")
		outputStr = strings.Trim(outputStr, "\"")
		return outputStr
	}

	return ""
}
