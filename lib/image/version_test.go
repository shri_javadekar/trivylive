package image

import (
	"strings"
	"testing"

	"github.com/onsi/gomega"
	"k8s.io/klog/klogr"
)

func TestImageVersionCheck(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	log := klogr.New()

	image := "hjacobs/kube-janitor:20.4.0"
	out := CheckNewVersion(image, log)
	g.Expect(out).NotTo(gomega.Equal(""))
	g.Expect(strings.HasPrefix(out, "New versions for "+image)).To(gomega.BeTrue())

	// Even for non-existent images, there should be some output
	image = "something:nonexistent"
	out = CheckNewVersion(image, log)
	g.Expect(out).NotTo(gomega.Equal(""))

	// Latest images
	image = "gcr.io/spark-operator/spark-operator:latest"
	out = CheckNewVersion(image, log)
	g.Expect(out).NotTo(gomega.Equal(""))

	// If no version is specified, the version check returns nothing
	image = "abcd"
	out = CheckNewVersion(image, log)
	g.Expect(out).To(gomega.Equal(""))
}
