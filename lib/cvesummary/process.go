package cvesummary

import (
	"amplifykube.com/trivylive/api/v1alpha1"
	"github.com/go-logr/logr"
)

func Update(vulnerabilities *[]v1alpha1.Vulnerability, image string, log logr.Logger) {
	for _, vuln := range *vulnerabilities {
		log.Info("Result", "image", image, "Vulnerabilities", len(*vulnerabilities), "CVE", vuln.CVE)

		if v1alpha1.CVESummary[vuln.CVE] == nil {
			v1alpha1.CVESummary[vuln.CVE] = make([]string, 0)
		}

		var found bool
		for _, img := range v1alpha1.CVESummary[vuln.CVE] {
			if img == image {
				found = true
				break
			}
		}

		if !found {
			v1alpha1.CVESummary[vuln.CVE] = append(v1alpha1.CVESummary[vuln.CVE], image)
		}
	}
}
