package report

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"amplifykube.com/trivylive/api/v1alpha1"
	"github.com/onsi/gomega"
	"k8s.io/klog/klogr"
)

func TestWrite(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	log := klogr.New()

	namespace := "default"
	dirPath, err := ioutil.TempDir("/tmp", "trivy-summary-test-")
	if err != nil {
		t.Fail()
	}
	defer os.RemoveAll(dirPath)

	var ivss []v1alpha1.ImageVulnerabilitySummary

	// Even if the imageScanSummaries array is empty, the summary should be created.
	nsVS, err := WriteNSReport(namespace, dirPath, &ivss, log)
	g.Expect(err).To(gomega.BeNil())
	g.Expect(nsVS).NotTo(gomega.BeNil())
	fileInfo, err := os.Stat(filepath.Join(dirPath, NSSummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Fail()
	}

	for i := 0; i < 2; i++ {
		ivs := v1alpha1.ImageVulnerabilitySummary{
			Image: fmt.Sprintf("abcd:%v", i),
			Count: i,
		}

		ivss = append(ivss, ivs)
	}

	nsVS, err = WriteNSReport(namespace, dirPath, &ivss, log)
	g.Expect(err).To(gomega.BeNil())
	g.Expect(nsVS).NotTo(gomega.BeNil())
	fileInfo, err = os.Stat(filepath.Join(dirPath, NSSummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Fail()
	}

	g.Expect(fileInfo.Size()).To(gomega.Equal(int64(206)))

	// Add the same images again to ivss and write the report again.
	// It should stay the same since they should get deduped.
	for i := 0; i < 2; i++ {
		ivs := v1alpha1.ImageVulnerabilitySummary{
			Image: fmt.Sprintf("abcd:%v", i),
			Count: i,
		}

		ivss = append(ivss, ivs)
	}
	g.Expect(len(ivss)).To(gomega.Equal(4))
	nsVS, err = WriteNSReport(namespace, dirPath, &ivss, log)
	g.Expect(err).To(gomega.BeNil())
	g.Expect(nsVS).NotTo(gomega.BeNil())
	fileInfo, err = os.Stat(filepath.Join(dirPath, NSSummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Fail()
	}
	g.Expect(fileInfo.Size()).To(gomega.Equal(int64(206)))
}

func TestWriteOverallReport(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	log := klogr.New()

	var nsReports []v1alpha1.NamespaceVulnerabilitySummary
	for i := 0; i < 2; i++ {
		nsr := v1alpha1.NamespaceVulnerabilitySummary{
			Namespace:          fmt.Sprintf("ns-%v", i),
			VulnerabilityCount: 3,
			ImageCount:         i,
		}
		nsReports = append(nsReports, nsr)
	}

	dirPath, err := ioutil.TempDir("/tmp", "trivy-scan-test-report-")
	if err != nil {
		t.Log("failed to create temporary directory")
		t.Fail()
	}
	defer os.RemoveAll(dirPath)

	WriteOverallReport(time.Now(), &nsReports, dirPath, log)
	fileInfo, err := os.Stat(filepath.Join(dirPath, LastScanSummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Log("latestScanSummary did not get created")
		t.Fail()
	}
	g.Expect(fileInfo.Size()).To(gomega.Equal(int64(288)))

	fileInfo, err = os.Stat(filepath.Join(dirPath, CVESummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Log("CVESummary did not get created")
		t.Fail()
	}
	g.Expect(fileInfo.Size()).To(gomega.Equal(int64(2)))

	// Test html diff
	// Update one of the NamespaceVulnerabilitySummary
	nsReports[0].ImageCount = 3
	nsReports[0].VulnerabilityCount = 20

	// Call WriteOverallReport again. This time the previous summary should get
	// renamed to the prevScanReport and an html report should be generated.
	WriteOverallReport(time.Now(), &nsReports, dirPath, log)
	fileInfo, err = os.Stat(filepath.Join(dirPath, LastScanSummaryFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Log("latestScanSummary did not get created")
		t.Fail()
	}
	g.Expect(fileInfo.Size()).To(gomega.Equal(int64(289)))

	fileInfo, err = os.Stat(filepath.Join(dirPath, HTMLDiffFileName))
	if errors.Is(err, os.ErrNotExist) {
		t.Log("htmldiff not generated")
		t.Fail()
	}

	// Confirm that the prevSummaryFile has been deleted.
	fileInfo, err = os.Stat(filepath.Join(dirPath, PrevScanSummaryFileName))
	if !errors.Is(err, os.ErrNotExist) {
		t.Log("previous file not deleted")
		t.Fail()
	}
}
