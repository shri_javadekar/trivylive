package report

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"time"

	"amplifykube.com/trivylive/api/v1alpha1"
	"github.com/go-logr/logr"
)

const (
	CVESummaryFileName      = "cveSummary.json"
	NSSummaryFileName       = "summary.json"
	LastScanSummaryFileName = "latestScanSummary.json"
	PrevScanSummaryFileName = "prevScanSummary.json"
	HTMLDiffFileName        = "summaryDiff.html"
)

func WriteNSReport(namespace string, dirPath string, imageScanSummaries *[]v1alpha1.ImageVulnerabilitySummary, log logr.Logger) (*v1alpha1.NamespaceVulnerabilitySummary, error) {
	var uniqueIvs []v1alpha1.ImageVulnerabilitySummary
	nsVS := v1alpha1.NamespaceVulnerabilitySummary{
		Namespace: namespace,
	}
	for idx := range *imageScanSummaries {
		// Check if this imageScanSummary is in uniqueIvs already.
		var found bool
		for udx := range uniqueIvs {
			if (*imageScanSummaries)[idx].Image == uniqueIvs[udx].Image {
				// Already exists; break
				log.Info("Image summary already present. Ignoring duplicates", "image", uniqueIvs[udx].Image)
				found = true
				break
			}
		}

		if !found {
			uniqueIvs = append(uniqueIvs, (*imageScanSummaries)[idx])
			nsVS.ImageCount = nsVS.ImageCount + 1
			nsVS.VulnerabilityCount = nsVS.VulnerabilityCount + (*imageScanSummaries)[idx].Count
		}
	}

	var file []byte
	if len(uniqueIvs) > 0 {
		file, _ = json.MarshalIndent(uniqueIvs, "", " ")
	} else {
		file = []byte("No vulnerabilities in this namespace")
	}
	err := ioutil.WriteFile(fmt.Sprintf("%v/%v", dirPath, NSSummaryFileName), file, 0644)
	if err != nil {
		return &nsVS, err
	}
	return &nsVS, nil
}

func WriteOverallReport(scanStartTime time.Time, nsReports *[]v1alpha1.NamespaceVulnerabilitySummary, rootPath string, log logr.Logger) {
	// 1. Write the CVESummary
	cves, _ := json.MarshalIndent(v1alpha1.CVESummary, "", " ")
	cveSummaryFilePath := fmt.Sprintf("%v/%v", rootPath, CVESummaryFileName)
	err := ioutil.WriteFile(cveSummaryFilePath, cves, 0644)
	if err != nil {
		log.Error(err, "failed to write cvesummary")
	}

	overAllSummaryObj := v1alpha1.OverallScanSummary{
		StartTime: scanStartTime.Format(time.RFC1123),
		EndTime:   time.Now().Format(time.RFC1123),
		NSReports: nsReports,
	}
	file, _ := json.MarshalIndent(overAllSummaryObj, "", " ")

	prevFilePath := fmt.Sprintf("%v/%v", rootPath, PrevScanSummaryFileName)
	latestFilePath := fmt.Sprintf("%v/%v", rootPath, LastScanSummaryFileName)
	var prevCreated bool
	// 2. Rename old reports file as prevScanSummaryFile.json
	err = os.Rename(latestFilePath, prevFilePath)
	prevCreated = (err == nil)
	if err != nil {
		log.Error(err, "failed to rename file. html diff may not be created")
	}

	// 3. Write the new report
	err = ioutil.WriteFile(latestFilePath, file, 0644)
	if err != nil {
		log.Error(err, "failed to write overall report")
	}

	if !prevCreated {
		return
	}

	// 4. Create the diff
	htmlDiffPath := fmt.Sprintf("%v/%v", rootPath, HTMLDiffFileName)

	cmd := fmt.Sprintf("diff -s -u %s %s  | diff2html -s side -F %s -i stdin", prevFilePath, latestFilePath, htmlDiffPath)
	out, err := exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		log.Error(err, "failed to generate html diff")
	} else {
		log.Info("html diff generated successfully", "output", string(out))
	}

	// 5. Delete prevScanSummaryFile.json
	os.Remove(prevFilePath)
}
