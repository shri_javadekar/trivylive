package setup

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"time"

	"github.com/go-logr/logr"
)

func deleteOldDirs(retention int, dirPath string, log logr.Logger) {
	var dirs []os.FileInfo
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		log.Error(err, "failed to list dirs")
		return
	}

	for idx := range files {
		if files[idx].IsDir() {
			dirs = append(dirs, files[idx])
		}
	}

	sort.Slice(dirs, func(i, j int) bool {
		return dirs[i].ModTime().Unix() < dirs[j].ModTime().Unix()
	})

	if retention <= 0 || len(dirs) < retention {
		return
	}

	dirs = dirs[:len(dirs)-retention]
	for idx := range dirs {
		os.RemoveAll(filepath.Join(dirPath, dirs[idx].Name()))
	}
}

func createNewDir(namespace string, dirPath string, log logr.Logger) (string, error) {
	t := time.Now()
	date := fmt.Sprintf("%v-%v-%v-%v", t.Day(), t.Format("Jan"), t.Year(), t.Hour())

	prefix := fmt.Sprintf("%v/%v/%v", dirPath, date, namespace)
	log.Info("Creating dir structure", "path", prefix)

	// No need to check if the dir already exists.
	err := os.MkdirAll(prefix, 0755)
	if err != nil {
		log.Error(err, "failed to create dir structure")
		return "", err
	}

	return prefix, nil
}

func Run(namespace string, dirPath string, retention int, log logr.Logger) (string, error) {
	prefix, err := createNewDir(namespace, dirPath, log)

	deleteOldDirs(retention, dirPath, log)

	return prefix, err
}
