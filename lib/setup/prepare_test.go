package setup

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/onsi/gomega"
	"k8s.io/klog/klogr"
)

func TestSetupRun(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	dir, err := ioutil.TempDir("/tmp", "trivy-scan-test-")
	if err != nil {
		t.Fail()
	}
	defer os.RemoveAll(dir)

	prefix, err := Run("abcd", dir, -1, klogr.New())
	g.Expect(err).To(gomega.BeNil())

	tm := time.Now()
	date := fmt.Sprintf("%v-%v-%v-%v", tm.Day(), tm.Format("Jan"), tm.Year(), tm.Hour())
	expectedPrefix := fmt.Sprintf("%v/%v/abcd", dir, date)
	g.Expect(prefix).To(gomega.Equal(expectedPrefix))

	os.RemoveAll(expectedPrefix)
}

func TestRetention(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	dir, err := ioutil.TempDir("/tmp", "trivy-scan-test-")
	if err != nil {
		t.Log("error creating temporary directory")
		t.Fail()
	}
	defer os.RemoveAll(dir)

	// Create some random files
	d1 := []byte("some random data\n")
	for i := 0; i < 3; i++ {
		err = os.WriteFile(filepath.Join(dir, fmt.Sprintf("temp-file-%v", i)), d1, 0644)
		if err != nil {
			t.Log("Failed to create temporary file")
			t.Fail()
		}
	}

	// Create 10 directories in dir
	for i := 0; i < 10; i++ {
		os.MkdirAll(filepath.Join(dir, fmt.Sprintf("test-dir-%v", i)), 0755)
	}

	subdirs, err := ioutil.ReadDir(dir)
	if err != nil {
		t.Log("error reading dir")
		t.Fail()
	}
	g.Expect(len(subdirs)).To(gomega.Equal(10 + 3))

	// Call deleteOldDirs with retention 0. There should still be 10 subdirs.
	deleteOldDirs(0, dir, klogr.New())
	subdirs, err = ioutil.ReadDir(dir)
	if err != nil {
		t.Log("error reading dir with retention 0")
		t.Fail()
	}
	g.Expect(len(subdirs)).To(gomega.Equal(10 + 3))

	// Call deleteOldDirs with retention -1. There should still be 10 subdirs.
	deleteOldDirs(-1, dir, klogr.New())
	subdirs, err = ioutil.ReadDir(dir)
	if err != nil {
		t.Log("error reading dir with retention -1")
		t.Fail()
	}
	g.Expect(len(subdirs)).To(gomega.Equal(10 + 3))

	// Call deleteOldDirs with retention 20. There should still be 10 subdirs.
	deleteOldDirs(20, dir, klogr.New())
	subdirs, err = ioutil.ReadDir(dir)
	if err != nil {
		t.Log("error reading dir with retention 20")
		t.Fail()
	}
	g.Expect(len(subdirs)).To(gomega.Equal(10 + 3))

	// Call deleteOldDirs with retention 8. There should still be 8 subdirs.
	deleteOldDirs(8, dir, klogr.New())
	subdirs, err = ioutil.ReadDir(dir)
	if err != nil {
		t.Log("error reading dir with retention 8")
		t.Fail()
	}
	g.Expect(len(subdirs)).To(gomega.Equal(8 + 3))

	deleteOldDirs(2, dir, klogr.New())
	subdirs, err = ioutil.ReadDir(dir)
	if err != nil {
		t.Log("error reading dir with retention 2")
		t.Fail()
	}
	g.Expect(len(subdirs)).To(gomega.Equal(2 + 3))

	// The test files created at the start should still be there.
	for i := 0; i < 3; i++ {
		_, err := os.Stat(filepath.Join(dir, fmt.Sprintf("temp-file-%v", i)))
		if errors.Is(err, os.ErrNotExist) {
			t.Log("temp files got deleted?")
			t.Fail()
		}
	}
}
