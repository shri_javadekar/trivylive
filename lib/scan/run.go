package scan

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"amplifykube.com/trivylive/api/v1alpha1"
	"amplifykube.com/trivylive/lib/cvesummary"
	img "amplifykube.com/trivylive/lib/image"
	"github.com/go-logr/logr"
)

func runTrivy(image string, imageID string, prefix string, format v1alpha1.ReportFormat, log logr.Logger) (string, error) {
	cmd := exec.Command("trivy", "i", "-f", fmt.Sprintf("%s", format), image)
	cmd.Env = os.Environ()
	suffix := strings.Split(imageID, "sha256:")[1] //TODO: This might be fragile?
	outputFile := fmt.Sprintf("%v/trivy-results-%v.%v", prefix, suffix, v1alpha1.ReportFormatToFileExt[format])

	if _, err := os.Stat(outputFile); errors.Is(err, os.ErrNotExist) {
		cmd.Env = append(cmd.Env, "TRIVY_OUTPUT="+outputFile, "TRIVY_SEVERITY=CRITICAL")
		out, err := cmd.CombinedOutput()
		// TODO: Maybe do some additional filtering here?
		if err != nil {
			log.Error(err, "Failed to run trivy", "image", image)
			log.Info(string(out))
			return "", err
		}
	} else {
		log.Info("Container already scanned", "image", image)
	}

	return outputFile, nil
}

func parseScanResult(filePath string, imageID string, log logr.Logger) *v1alpha1.ImageVulnerabilitySummary {
	var scanOutput v1alpha1.ScanOutput
	b, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Error(err, "failed to read scan results")
		return nil
	}

	// TODO: Check for errors here
	json.Unmarshal(b, &scanOutput)

	var vulnerabilityCount int
	if len(scanOutput.Results) > 0 {
		vulnerabilityCount = len(scanOutput.Results[0].Vulnerabilities)
		log.Info("Result", "image", scanOutput.ArtifactName, "Vulnerabilities", len(scanOutput.Results[0].Vulnerabilities))
		cvesummary.Update(&scanOutput.Results[0].Vulnerabilities, scanOutput.ArtifactName, log)
	}

	return &v1alpha1.ImageVulnerabilitySummary{
		Image:          scanOutput.ArtifactName,
		ImageID:        imageID,
		FullReportPath: filePath,
		Count:          vulnerabilityCount,
	}
}

func Run(image string, imageID string, prefix string, format v1alpha1.ReportFormat, log logr.Logger) *v1alpha1.ImageVulnerabilitySummary {
	// Always run trivy with the json format output to create the imageVulnerabilitySummary object
	outputFile, err := runTrivy(image, imageID, prefix, v1alpha1.ReportFormatJson, log)
	if err != nil {
		log.Error(err, "Failed to run trivy with json output")
		return nil
	}
	ivs := parseScanResult(outputFile, imageID, log)

	// Check if there is a newer version available
	(*ivs).NewVersion = img.CheckNewVersion(image, log)

	// if the output format is json, nothing to do.
	if format == v1alpha1.ReportFormatJson {
		return ivs
	}

	// If the report format is not json
	// 1. run trivy again with table output format
	jsonOutputFile := outputFile
	outputFile, err = runTrivy(image, imageID, prefix, format, log)
	if err != nil {
		log.Error(err, "Failed to run trivy")
		return nil
	}

	// 2. delete the json file
	os.Remove(jsonOutputFile)

	// 3. change the name of the file in ivs
	ivs.FullReportPath = outputFile

	return ivs
}
