package scan

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/onsi/gomega"
	"k8s.io/klog/klogr"
)

func TestScanRun(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	dirPath, err := ioutil.TempDir("/tmp", "trivy-scan-test-")
	if err != nil {
		t.Fail()
	}
	defer os.RemoveAll(dirPath)
	image := "nginx:latest"

	os.MkdirAll(dirPath, 0755)
	defer os.RemoveAll(dirPath)

	ivs := Run(image, "something@sha256:abcd123456", dirPath, "json", klogr.New())
	g.Expect(ivs).NotTo(gomega.BeNil())
	g.Expect(ivs.Image).To(gomega.Equal(image))

	if _, err := os.Stat(filepath.Join(dirPath, "trivy-results-abcd123456.json")); errors.Is(err, os.ErrNotExist) {
		t.Fail()
	}

	// Try with table format
	ivs = Run(image, "something@sha256:abcd123456", dirPath, "table", klogr.New())
	g.Expect(ivs).NotTo(gomega.BeNil())
	g.Expect(ivs.Image).To(gomega.Equal(image))

	// json report should *not* exist
	_, err = os.Stat(filepath.Join(dirPath, "trivy-results-abcd123456.json"))
	g.Expect(err).NotTo(gomega.BeNil())
	if _, err := os.Stat(filepath.Join(dirPath, "trivy-results-abcd123456.txt")); errors.Is(err, os.ErrNotExist) {
		t.Fail()
	}

	// Run the report with a format that's neither json nor table.
	ivs = Run(image, "something@sha256:abcd123456", dirPath, "dfgdfg", klogr.New())
	g.Expect(ivs).To(gomega.BeNil())

	// Try running trivy on a non-existent image
	ivs = Run("something:nonexistent", "something@sha256:pqrs2343232", dirPath, "dfgdfg", klogr.New())
	g.Expect(ivs).To(gomega.BeNil())
}
