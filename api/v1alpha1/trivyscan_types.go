package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	FileSystemType = "filesystem"
)

type ReportFormat string

type TrivyScanState string

const (
	ReportFormatJson  ReportFormat   = "json"
	ReportFormatTable ReportFormat   = "table"
	TrivyScanRunning  TrivyScanState = "running"
)

var ReportFormatToFileExt = map[ReportFormat]string{
	ReportFormatJson:  "json",
	ReportFormatTable: "txt",
}

// ReportDetails defines the details for creating reports
type ReportDetails struct {
	Format ReportFormat `json:"format,omitempty"`
	Type   string       `json:"type,omitempty"`
	Path   string       `json:"path,omitempty"`
}

type RetentionDetails struct {
	Last int `json:"last"`
}

// TrivyScanSpec defines the desired state of TrivyScan
type TrivyScanSpec struct {
	RepeatDuration string           `json:"repeatDuration"`
	Report         ReportDetails    `json:"report,omitempty"`
	Retention      RetentionDetails `json:"retention"`
}

// TrivyScanStatus defines the observed state of TrivyScan
type TrivyScanStatus struct {
	State TrivyScanState `json:"state"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// TrivyScan is the Schema for the trivyscans API
type TrivyScan struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TrivyScanSpec   `json:"spec,omitempty"`
	Status TrivyScanStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// TrivyScanList contains a list of TrivyScan
type TrivyScanList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []TrivyScan `json:"items"`
}
type NamespaceVulnerabilitySummary struct {
	Namespace          string `json:"namespace"`
	ImageCount         int    `json:"imagecount"`
	VulnerabilityCount int    `json:"vulnerabilitycount"`
}

type ImageVulnerabilitySummary struct {
	Image          string `json:"image"`
	ImageID        string `json:"imageId"`
	FullReportPath string `json:"fullreportpath"`
	Count          int    `json:"count"`
	NewVersion     string `json:"newversion"`
}

type Vulnerability struct {
	Severity string `json:"Severity"`
	CVE      string `json:"VulnerabilityID"`
}

type Result struct {
	Target          string          `json:"Target"`
	Vulnerabilities []Vulnerability `json:"Vulnerabilities"`
}

type ScanOutput struct {
	SchemaVersion string   `json:"SchemaVersion"`
	ArtifactName  string   `json:"ArtifactName"`
	ArtifactType  string   `json:"ArtifactType"`
	Results       []Result `json:"Results"`
}

type OverallScanSummary struct {
	StartTime string                           `json:"ScanStartTime"`
	EndTime   string                           `json:"ScanEndTime"`
	NSReports *[]NamespaceVulnerabilitySummary `json:"NamespaceReports"`
}

var CVESummary map[string][]string

func init() {
	SchemeBuilder.Register(&TrivyScan{}, &TrivyScanList{})
	CVESummary = make(map[string][]string)
}
